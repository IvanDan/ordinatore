import org.apache.commons.io.FilenameUtils;

import java.io.*;

public class Ordinatore {
    private String estensione;
    private File fileCorrente;
    private File cartellaCorrente;
    private File[] listaFile;

    /**
     * Questo programma legge i file presenti nella cartella, crea le cartelle con
     * il nome delle estensione dei file e riorganizza spostando i file in base alla
     * loro estensione nelle rispettive cartelle
     *
     * @param percorso il percorso della cartella da ordinare
     */
    public Ordinatore(String percorso) {
        cartellaCorrente = new File(percorso);
        listaFile = cartellaCorrente.listFiles();
        ordina();
    }

    private void setEstensione() {
        estensione = FilenameUtils.getExtension(fileCorrente.getName());
    }

    private void ordina() {
        if (listaFile == null) {
            System.out.println("Il percorso non e' valido");
            return;
        } else if (listaFile.length == 0) {
            System.out.println("Nulla da ordinare");
            return;
        }
        for (File file : listaFile) {
            if (file.isFile()) {
                fileCorrente = file;
                setEstensione();
                new File(cartellaCorrente.getPath() + "/" + estensione).mkdir();
                if (file.renameTo(new File(cartellaCorrente.getPath() + "/" + estensione + "/" + file.getName()))) {
                    file.delete();
                } else {
                    System.err.println("Il file: " + fileCorrente.getName() + " non e' stato spostato perche' probabilmente e' in uso, oppure esiste gia' nella cartella");
                }
            }
        }
    }

    public static void main(String[] args) {
        new Ordinatore(args[0]);
    }
}
